const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.common');
const PATHS = require('./paths');

const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = merge(baseConfig, {
  mode: 'production',
  output: {
    path: PATHS.DIST,
    filename: '[name].[chunkhash].js'
  },
  plugins: [
    new CleanWebpackPlugin(PATHS.DIST, {
      root: path.resolve('./'),
      verbose: true,
      dry: false
    })
  ]
});
