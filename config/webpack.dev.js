const merge = require('webpack-merge');
const baseConfig = require('./webpack.common');
const PATHS = require('./paths');

module.exports = merge(baseConfig, {
  mode: 'development',
  output: {
    publicPath: '/'
  },
  devServer: {
    contentBase: PATHS.SRC,
    port: 8080,
    historyApiFallback: true,
    open: true,
  },
  devtool: 'cheap-eval-source-map',
})
