const path = require('path');
const PATHS = require('./paths');

module.exports = {
  "@shared": path.resolve(PATHS.APP, 'shared'),
  "@models": path.resolve(PATHS.APP, 'shared', 'models'),
  "@constants": path.resolve(PATHS.APP, 'shared', 'constants'),
  "@chairs": path.resolve(PATHS.APP, 'Chairs')
};
