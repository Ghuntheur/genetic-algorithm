const path = require('path');
const PATHS = require('./paths');
const ALIAS = require('./alias');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    bundle: [path.join(PATHS.SRC, 'index.ts'), path.join(PATHS.STYLES, 'index.scss')]
  },
  resolve: {
    modules: ['node_modules', PATHS.SRC],
    extensions: ['.ts', '.js', '.json', '.scss', '.css'],
    alias: {
      ...ALIAS,
      three$: 'three/build/three.min.js',
      'three/.*$': 'three'
    }
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: '/node_modules/',
        loader: 'babel-loader'
      },
      {
        test: /\.ts$/,
        enforce: 'pre',
        loader: 'tslint-loader'
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(PATHS.SRC, 'index.html'),
      filename: 'index.html',
      inject: 'body'
    }),
    new webpack.ProvidePlugin({
      THREE: 'three'
    })
  ]
};
