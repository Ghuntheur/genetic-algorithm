const path = require('path');

module.exports = {
  ROOT: path.resolve(__dirname, '../'),
  SRC: path.resolve(__dirname, '../', 'src'),
  APP: path.resolve(__dirname, '../', 'src', 'app'),
  DIST: path.resolve(__dirname, '../', 'dist'),
  ASSETS: path.resolve(__dirname, '../', 'src', 'assets'),
  STYLES: path.resolve(__dirname, '../', 'src', 'styles')
};
