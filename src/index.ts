import Main from './app/Main';

window.addEventListener('load', async () => {
  const app = new Main();
  await app.init();
  app.run();
});
