import * as THREE from 'three';

import SceneWrapper from './scene/SceneWrapper';

import Population from './Population';

class Main {

  sceneWrapper: SceneWrapper;

  population: Population;

  constructor(chairsCount: number = 100) {
    this.sceneWrapper = new SceneWrapper();
  }

  async init() {
    const scene = await this.sceneWrapper.init();
    this.sceneWrapper.animate();

    this.population = new Population(scene);
  }

  run() {
    this.population.iterate();
  }

}

export default Main;
