import * as THREE from 'three';

import Chair from './Chair';

import { IGene } from '@models/gene.model';

import { chairTemplateConstants } from '@constants/chair-template.constants';
import Utils from '@shared/Utils';

class ChairTemplate extends Chair {

  static MIN_FITNESS_REQUIRED: number = 0.4;

  constructor() {
    super();

    this.parameters = chairTemplateConstants;

    this.overrideGenotype();
    this.createChromosome();
  }

  private overrideGenotype() {
    this.genotype.forEach((gene: IGene) => {
      const int = this.parameters[gene.name];
      const valueMapped = Utils.mapInterval(int, gene.range.min, gene.range.max, 0, gene.range.max - gene.range.min);
      const binary: string = Utils.intToBinary(valueMapped, gene.length);
      gene.value = binary;
    });
  }

  render(scene: THREE.Scene) {
    super.render(scene);
    this.group.scale.set(7, 7, 7);
  }

}

export default ChairTemplate;
