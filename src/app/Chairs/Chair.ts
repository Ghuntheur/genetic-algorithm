import * as THREE from 'three';

import { IGene } from '@models/gene.model';
import { IRange } from '@models/range.model';
import { IChairParameters } from '@models/chair-parameters.model';

import Utils from '@shared/Utils';
import ChairTemplate from './ChairTemplate';

class Chair {
  static legsCountRange: IRange = { min: 1, max: 10 };
  static legsHeightRange: IRange = { min: 4, max: 24 };
  static sittingWidthRange: IRange = { min: 2, max: 30 };
  static sittingDepthRange: IRange = { min: 2, max: 30 };
  static backHeightRange: IRange = { min: 2, max: 40 };
  static backInclinationRange: IRange = { min: 0, max: 90 };
  static colorRange: IRange = { min: 0, max: 255 };

  static legsCountName: string = 'legsCount';
  static legsHeightName: string = 'legsHeight';
  static sittingWidthName: string = 'sittingWidth';
  static sittingDepthName: string = 'sittingDepth';
  static backHeightName: string = 'backHeight';
  static backInclinationName: string = 'backInclination';
  static redColorName: string = 'redColor';
  static greenColorName: string = 'greenColor';
  static blueColorName: string = 'blueColor';

  static mutationFrequency: number = 0.3;

  // assemble all chair elements in this group
  group: THREE.Group;

  // when chair genotype is decoded
  parameters: IChairParameters = {} as IChairParameters;

  // all genes and utils informations
  genotype: IGene[] = [];

  // binary representation
  chromosome: string;

  similarityRate: number;

  constructor() {
    this.group = new THREE.Group();

    this.initGenotype();
    this.calculateGenesLength();
  }

  createChromosome() {
    this.chromosome = this.genotype.map((gene: IGene) => gene.value).join('');
  }

  createGenotype() {
    this.genotype.forEach((gene: IGene) => {
      const random: number = Utils.randomInRange(gene.range);
      const valueMapped = Utils.mapInterval(random, gene.range.min, gene.range.max, 0, gene.range.max - gene.range.min);
      const binary: string = Utils.intToBinary(valueMapped, gene.length);
      gene.value = binary;
    });

    this.createChromosome();
  }

  encode() {
    this.genotype.reduce((acc, currentGene) => {
      const length = acc + currentGene.length;
      currentGene.value = this.chromosome.slice(acc, length);
      return length;
    }, 0);
  }

  decode() {
    this.genotype.forEach((gene: IGene) => {
      const value = Utils.binaryToInt(gene.value);
      const valueInInitialRange: number = Utils.mapInterval(value, 0, gene.range.max - gene.range.min, gene.range.min, gene.range.max);
      this.parameters[gene.name] = valueInInitialRange;
    });
  }

  evaluate(chairTemplate: ChairTemplate) {
    this.similarityRate = 1 - this.chromosome.split('').reduce((acc: number, val: string, index: number) => acc + (+val ^ +chairTemplate.chromosome.charAt(index)), 0) / this.chromosome.length;
  }

  cross(parentB: Chair): string[] {
    const parentA: Chair = this;
    const randomPosition = Utils.randomInRange({ min: 5, max: parentA.chromosome.length - 5 });

    const [leftA, rightA] = Utils.splitAt(parentA.chromosome, randomPosition);
    const [leftB, rightB] = Utils.splitAt(parentB.chromosome, randomPosition);

    return [`${leftA}${rightB}`, `${leftB}${rightA}`];
  }

  mutate() {
    const randomPosition: number = Utils.randomIndexInArray(this.chromosome.length);
    const bit: number = +this.chromosome.charAt(randomPosition);
    this.chromosome = this.chromosome.substring(0, randomPosition) + +!bit + this.chromosome.substring(randomPosition + 1);
  }

  render(scene: THREE.Scene, index: number = 0, radius: number = 0, angle: number = 0) {
    const color: string = `rgb(${this.parameters.redColor}, ${this.parameters.greenColor}, ${this.parameters.blueColor})`;

    const legsCount = this.parameters.legsCount;
    const legsRadius = 0.2;

    const sittingWidth = this.parameters.sittingWidth;
    const sittingDepth = this.parameters.sittingDepth;

    const backHeight = this.parameters.backHeight;

    // legs
    const legsGeometry = this.getLegGeometry(legsRadius);
    const legsMaterial = this.getLegMaterial(color);
    const legsMesh = new THREE.Mesh(legsGeometry, legsMaterial);
    legsMesh.position.y = this.parameters.legsHeight / 2;
    const circleRadius = (Math.min(sittingWidth, sittingDepth) - legsRadius) / 2;
    const legsAngle = 2 * Math.PI / legsCount;

    const legs: THREE.Group = new THREE.Group();
    for (let i = 0; i < legsCount; i++) {
      const mesh = legsMesh.clone();
      mesh.position.x = circleRadius * Math.cos(legsAngle * i);
      mesh.position.z = circleRadius * Math.sin(legsAngle * i);
      legs.add(mesh);
    }
    legs.rotateY(Math.PI / 4);
    this.group.add(legs);

    // sitting
    const sittingGeometry = new THREE.BoxGeometry(sittingWidth, 0.4, sittingDepth);
    const sittingMaterial = new THREE.MeshBasicMaterial({ color });
    const sittingMesh = new THREE.Mesh(sittingGeometry, sittingMaterial);
    sittingMesh.position.y = legsMesh.position.y * 2 + 0.2;
    this.group.add(sittingMesh);

    // chairBack
    const backGeometry = new THREE.BoxGeometry(sittingWidth, backHeight, 0.4);
    const backMaterial = new THREE.MeshBasicMaterial({ color });
    const backMesh = new THREE.Mesh(backGeometry, backMaterial);

    backMesh.geometry.translate(0, backHeight / 2, 0);
    backMesh.position.y = sittingMesh.position.y;
    backMesh.position.z -= sittingDepth / 2;
    backMesh.rotateX(THREE.Math.degToRad(-this.parameters.backInclination));

    this.group.add(backMesh);

    this.moveChairOnCircle(this.group, index, radius, angle);

    scene.add(this.group);
  }

  remove(scene: THREE.Scene) {
    scene.remove(this.group);
  }

  private moveChairOnCircle(chair: THREE.Group, index: number, radius: number, angle: number) {
    chair.position.x = radius * Math.cos(angle * index);
    chair.position.z = radius * Math.sin(angle * index);
    chair.lookAt(0, 0, 0);
  }

  private initGenotype() {
    this.genotype = [
      { name: Chair.legsCountName, range: Chair.legsCountRange },
      { name: Chair.legsHeightName, range: Chair.legsHeightRange },
      { name: Chair.sittingWidthName, range: Chair.sittingWidthRange },
      { name: Chair.sittingDepthName, range: Chair.sittingDepthRange },
      { name: Chair.backHeightName, range: Chair.backHeightRange },
      { name: Chair.backInclinationName, range: Chair.backInclinationRange },
      { name: Chair.redColorName, range: Chair.colorRange },
      { name: Chair.greenColorName, range: Chair.colorRange },
      { name: Chair.blueColorName, range: Chair.colorRange }
    ];
  }

  private calculateGenesLength() {
    this.genotype.forEach((gene: IGene) => {
      const range = gene.range.max - gene.range.min;
      const rangeBinary = Utils.intToBinary(range);
      gene.length = rangeBinary.length;
    });
  }

  private getLegGeometry(legsRadius: number): THREE.CylinderGeometry {
    const height = this.parameters.legsHeight;
    return new THREE.CylinderGeometry(legsRadius, legsRadius, height, 16);
  }

  private getLegMaterial(color: string): THREE.MeshBasicMaterial {
    return new THREE.MeshBasicMaterial({ color });
  }

}

export default Chair;
