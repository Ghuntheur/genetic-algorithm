import * as THREE from 'three';
import 'three/examples/js/controls/OrbitControls';

class SceneWrapper {
  private width: number;
  private height: number;

  // HTMLElements
  private containerElements: HTMLElement = document.body;

  // render
  private renderer: THREE.WebGLRenderer;

  private scene: THREE.Scene;

  // camera
  private fov: number = 45;
  private aspect: number;
  private camera: THREE.PerspectiveCamera;
  private orbitControls: THREE.OrbitControls;

  constructor() {
    this.renderer = new THREE.WebGLRenderer({
      antialias: true
    });
    this.scene = new THREE.Scene();
  }

  init(): Promise<THREE.Scene> {
    return new Promise(resolve => {
      this.initScene();
      this.initCamera();
      this.initLights();
      this.bindEvents();

      // this.showAxesHelper();

      resolve(this.scene);
    });
  }

  animate() {
    requestAnimationFrame(() => this.animate());
    this.renderer.render(this.scene, this.camera);
  }

  private initScene() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.aspect = this.width / this.height;

    this.renderer.setSize(this.width, this.height);
    this.containerElements.appendChild(this.renderer.domElement);
  }

  private initLights() {

  }

  private initCamera() {
    this.camera = new THREE.PerspectiveCamera(this.fov, this.aspect, 1, 2000);
    this.camera.position.set(-700, 800, 1000);
    this.camera.lookAt(0, 0, 0);

    this.orbitControls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

    this.scene.add(this.camera);
  }

  private bindEvents() {
    window.addEventListener('resize', () => {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.aspect = this.width / this.height;
      this.camera.aspect = this.aspect;
      this.camera.updateProjectionMatrix();
      this.orbitControls.update();
      this.renderer.setSize(this.width, this.height);
    });
  }

  private showAxesHelper() {
    this.scene.add(new THREE.AxesHelper(20));
  }
}

export default SceneWrapper;
