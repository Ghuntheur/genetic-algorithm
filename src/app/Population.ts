import * as THREE from 'three';

import Chair from '@chairs/Chair';
import ChairTemplate from '@chairs/ChairTemplate';

import Utils from '@shared/Utils';

class Population {

  private fitnessElem: HTMLElement = document.querySelector('#fitness .value');
  private generationElem: HTMLElement = document.querySelector('#generation .value');

  chairs: Chair[] = [];
  chairTemplate: ChairTemplate;

  chrono: number;

  scene: THREE.Scene;

  constructor(scene: THREE.Scene, count: number = 100) {
    this.scene = scene;

    this.createChairs(count);
    this.chairTemplate = new ChairTemplate();
  }

  iterate() {
    let i = 0;
    this.chrono = setInterval(() => {
      this.decodeChairsGenotype();
      this.evaluateChairs();
      this.selectBestChairs();
      this.crossover();
      this.mutate();
      this.render();

      this.fitnessElem.textContent = this.calculateFitness().toPrecision(4);
      this.generationElem.textContent = (i++).toString();

      if (this.calculateFitness() === 1) {
        clearInterval(this.chrono);
      }

    }, 500);
  }

  private evaluateChairs() {
    this.chairs.forEach((chair: Chair) => {
      chair.evaluate(this.chairTemplate);
    });
  }

  private selectBestChairs() {
    const [aliveChairs, deletedChairs]: Chair[][] = Utils.partitionArray(this.chairs, (chair: Chair) => chair.similarityRate >= ChairTemplate.MIN_FITNESS_REQUIRED);
    aliveChairs.sort((a: Chair, b: Chair) => a.similarityRate - b.similarityRate);

    aliveChairs.forEach((chair: Chair) => chair.remove(this.scene));
    aliveChairs.splice(0, this.chairs.length / 2);

    deletedChairs.forEach((chair: Chair) => {
      chair.remove(this.scene);
      chair = null;
    });

    this.chairs = aliveChairs;
  }

  private crossover() {
    // shuffle chairs array
    const tmpChairs: Chair[] = [];
    this.chairs = Utils.shuffleArray(this.chairs);

    for (let i = 0; i < this.chairs.length - 1; i += 2) {
      const parentA: Chair = this.chairs[i];
      const parentB: Chair = this.chairs[i + 1];

      const [childA, childB] = parentA.cross(parentB);

      const chairA: Chair = this.createChild(childA);
      const chairB: Chair = this.createChild(childB);

      tmpChairs.push(chairA, chairB);
    }
    this.chairs = [...this.chairs, ...tmpChairs];
  }

  private mutate() {
    this.chairs.forEach((chair: Chair) => {
      const random = Math.random();
      if (random > Chair.mutationFrequency && this.calculateFitness() < 0.9) chair.mutate();
    });
  }

  private createChild(chromosome: string): Chair {
    const child = new Chair();
    child.chromosome = chromosome;
    child.encode();
    child.decode();

    child.evaluate(this.chairTemplate);

    return child;
  }

  private render() {
    const maxCirconference = (Chair.sittingWidthRange.max - Chair.sittingWidthRange.min) / 3 * this.chairs.length;
    const radius = maxCirconference / (Math.PI / 2);
    const angle = 2 * Math.PI / this.chairs.length;

    this.chairs.forEach((chair: Chair, index: number) => {
      chair.render(this.scene, index, radius, angle);
    });
    this.chairTemplate.render(this.scene);
  }

  private createChairs(count: number) {
    for (let i = 0; i < count; i++) {
      const chair = new Chair();
      chair.createGenotype();
      this.chairs.push(chair);
    }
  }

  private decodeChairsGenotype() {
    this.chairs.forEach((chair: Chair) => {
      chair.decode();
    });
  }

  private calculateFitness(): number {
    return this.chairs.reduce((acc: number, currentChair: Chair) => acc + currentChair.similarityRate, 0) / this.chairs.length;
  }

}

export default Population;
