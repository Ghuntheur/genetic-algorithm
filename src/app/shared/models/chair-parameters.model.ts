export interface IChairParameters {
  legsCount: number;
  legsHeight: number;
  sittingWidth: number;
  sittingDepth: number;
  backHeight: number;
  backInclination: number;
  redColor: number;
  greenColor: number;
  blueColor: number;
}
