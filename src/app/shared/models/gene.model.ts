import { IRange } from './range.model';

export interface IGene {
  name: string;
  range: IRange;
  length?: number;
  value?: string;
}
