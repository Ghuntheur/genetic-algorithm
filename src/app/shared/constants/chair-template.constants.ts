import { IChairParameters } from '@models/chair-parameters.model';

export const chairTemplateConstants: IChairParameters = {
  legsCount: 4,
  legsHeight: 7,
  sittingWidth: 8,
  sittingDepth: 8,
  backHeight: 9,
  backInclination: 10,
  redColor: 255,
  greenColor: 255,
  blueColor: 255
};
