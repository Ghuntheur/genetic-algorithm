import { IRange } from '@models/range.model';

class Utils {

  static randomInRange(range: IRange): number {
    return Math.floor(Math.random() * (range.max - range.min + 1)) + range.min;
  }

  static randomIndexInArray(arrayLength: number): number {
    return Math.floor(Math.random() * arrayLength);
  }

  static intToBinary(int: number, length?: number): string {
    if (!length) return int.toString(2);

    let out = '';
    while (length--) out += (int >> length) & 1;
    return out;
  }

  static binaryToInt(binary: string, base: number = 2): number {
    return parseInt(binary, base);
  }

  static mapInterval(value: number, a: number, b: number, c: number, d: number): number {
    return c + (d - c) / (b - a) * (value - a);
  }

  static partitionArray(array: any[], callback: Function): any[] {
    return array.reduce((acc, element) => {
      acc[callback(element) ? 0 : 1].push(element);
      return acc;
    }, [[], []]);
  }

  static shuffleArray(array: any[]): any[] {
    return array
      .map(a => [Math.random(), a])
      .sort((a, b) => a[0] - b[0])
      .map(a => a[1]);
  }

  // static splitAt = index => x => [x.slice(0, index), x.slice(index)];

  static splitAt(string: string, index: number): string[] {
    return [string.slice(0, index), string.slice(index)];
  }

}

export default Utils;
